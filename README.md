# Creates a Windows7 USB under Linux using Docker.

## Usage:

docker build -t=win7_usb .

docker run --rm --privileged --device=$USB_DEV:/dev/sdb:rw -v $WIN_ISO:/Windows7.iso win7_usb

where:

_$USB_DEV_: USB Device (usually /dev/sdX). Be careful, it will wipe out the whole device!!!

_$WIN_ISO_: Windows7 .iso file's absolute path (e.g.: /isos/win7.iso). You can easily get an .iso file using trorent.
