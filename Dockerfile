FROM ubuntu:latest
MAINTAINER Santiago Sanchez Paz (sanchezpaz@gmail.com)

RUN export DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get -y --no-install-recommends install ntfs-3g grub2 parted

ADD run.sh /run.sh

CMD /run.sh
