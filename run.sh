#!/bin/bash

set -e

[ -e /dev/sdb ] || (echo "No /dev/sdb device."; exit)

echo "1/4: Creating USB volume structure and filesystem.."
parted -s -a optimal /dev/sdb mklabel msdos -- mkpart primary ntfs 1 -1  set 1 boot on >/dev/null 2>&1
mkfs.ntfs -f /dev/sdb1 >/dev/null 2>&1

echo "2/4: Mounting USB and .iso file.."
mkdir /media/usb && mount /dev/sdb1 /media/usb
mkdir /media/win7 && mount -o loop /Windows7.iso /media/win7 >/dev/null 2>&1

echo "3/4: Copying files (it might take a while).."
cp -rp /media/win7/* /media/usb/ >/dev/null 2>&1

echo "4/4: Installing and configuring the boot-loader.."
grub-install --force --target=i386-pc --boot-directory="/media/usb/boot" /dev/sdb >/dev/null 2>&1

UUID=$(blkid /dev/sdb1 | cut -d'"' -f2)

echo """echo \"If you see this, you have successfully booted from USB :)\"
insmod ntfs
insmod search_fs_uuid
search --no-floppy --fs-uuid $UUID --set root
ntldr /bootmgr
boot""" > /media/usb/boot/grub/grub.cfg

umount /media/usb
